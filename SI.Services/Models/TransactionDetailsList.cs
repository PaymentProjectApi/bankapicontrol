﻿using System;
using System.Collections.Generic;

namespace SI.Services.Models
{
    public class TransactionDetailsList
    {
        public List<TransactionDetailsListItems> Items { get; } = new List<TransactionDetailsListItems>();
    }

    public class TransactionDetailsListItems
    {
        public Guid TransactionID { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionAmount { get; set; }
        public string TransactionDescription { get; set; }
        public string TransactionStatus { get; set; }
    }
}
