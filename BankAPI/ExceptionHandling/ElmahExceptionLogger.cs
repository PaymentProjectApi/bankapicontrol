﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using Elmah;

namespace BankAPI.ExceptionHandling
{
    public class ElmahExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            var logException = new Exception();
            logException = context.Exception;

            while (logException.InnerException != null)
            {
                var logError = new Elmah.Error();
                logError.Source = logException.Source;
                logError.Type = logException.GetType().ToString();
                logError.Message = logException.Message;
                logError.Time = DateTime.UtcNow;
                logError.HostName = System.Environment.MachineName;
                logError.User = HttpContext.Current.User.Identity.Name;
                ErrorLog.GetDefault(HttpContext.Current).Log(logError);
                logException = logException.InnerException;
            }

            ErrorSignal.FromCurrentContext().Raise(context.Exception);

        }
    }
}