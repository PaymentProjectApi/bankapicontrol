﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Elmah;
using System.Web.Http.ExceptionHandling;
using BankAPI.ExceptionHandling;

namespace BankAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //Elmah Logger
            config.Services.Replace(typeof(IExceptionLogger), new ElmahExceptionLogger());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
