﻿using SI.Services.Interfaces;
using SI.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BankAPI.Controllers
{
    public class CustomerController : ApiController
    {
        public ICustomerService _customerService { get; set; }
        public IPaymentService _paymentService { get; set; }

        public CustomerController(ICustomerService customerService, IPaymentService paymentService)
        {
            _customerService = customerService;
            _paymentService = paymentService;
        }


        

        // GET: api/Customer
        public IEnumerable<string> Get()
        {
            var customers = _customerService.GetListofCustomer();

           var x = customers.Items.Select(i => i.FirstName).FirstOrDefault();
            return new string[] { x, "value2" };
        }

        [ActionName("ProcessPayment")]
        public object Post(Guid customerID, string FName, string LName, string cardNumber, string expiry, string cvv, decimal balance)
        {
            var payment = _paymentService.AddTransaction(customerID,FName,LName,cardNumber,expiry,cvv,balance);

            if (payment.Status)
                return Ok(new { status = "Success", message = MessageResponses.GetMessage(payment.StatusCode) });

            else
                return Ok(new { status = "Failure", message = MessageResponses.GetMessage(payment.StatusCode) });
        }

        // GET: api/Customer/5
        public string Get(Guid id)
        {
            //var customers = _customerService.GetSpecificCustomerDetails(id);

            return "value";
        }

        // POST: api/Customer
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Customer/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Customer/5
        public void Delete(int id)
        {
        }
    }
}
