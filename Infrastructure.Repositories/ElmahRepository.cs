﻿using Elmah;
using System;


namespace Infrastructure.Repositories
{
    public class ElmahRepository
    {
        public static void ElmahLog(Exception exception, string userid)
        {
            var logException = new Exception();
            logException = exception;
            // logException = context.Exception;

            while (logException.InnerException != null)
            {
                var logError = new Elmah.Error();
                logError.Source = logException.Source;
                logError.Type = logException.GetType().ToString();
                logError.Message = logException.Message;
                logError.Time = DateTime.UtcNow;
                logError.HostName = Environment.MachineName;
                logError.User = userid;
                ErrorLog.GetDefault(null).Log(logError);
                logException = logException.InnerException;
            }
            ErrorLog.GetDefault(null).Log(new Elmah.Error(exception));
        }
    }
}
