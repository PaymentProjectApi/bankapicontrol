namespace Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Domain.Entities;

    public partial class BankContext : DbContext
    {
        public BankContext()
            : base("name=BankContext")
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //map baseEntity on primary key of tables
            modelBuilder.Entity<Customer>()
                .Property(b => b.Id)
                .HasColumnName("CustomerID");

            modelBuilder.Entity<Transaction>()
                .Property(b => b.Id)
                .HasColumnName("TransactionID");
        }
    }
}
