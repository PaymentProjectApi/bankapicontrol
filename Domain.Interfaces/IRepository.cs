﻿using Domain.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        T GetSingle(object ID);

        void Add(T entity);

        void Delete(T entity);

        void Update(T entity);
    }
}
